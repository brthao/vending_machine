
# VENDING MACHINE

One approach to the resolution of the problem : return the most coin possible
 for the change of a vending machine transaction. For QIM Info.

Using maven.
Java version : 13.

### Using TreeMap :

Useful in this problem because it is a sorted map. 
And we want to iterate from the highest value coin to the lowest.
We can use methods like :
- lastKey(): return the lastKey of the map (here => coin of the highest value),
- lowerKey(k: Key): return the key just before k in the TreeMap 
    (here => highest value coin under k),


## Algorithm (returnChangeMaxCoins):

##### Constraints : 
######    All products have the same price: ALL_PRODUCTS_PRICE: float = 0.80

##### Input : _coins: Map, _customerInput: float

```
SORT _coins : using TreeMap.

CALCULATE the total money in the vending machine: _total: float.

CALCULATE change : _change = _customerInput - ALL_PRODUCTS_PRICE.

_highestValueCoin = highest value coin in _coins

WHILE _total != _change:
    IF number of coins of _highestValueCoin == 0:
        _highestValueCoin = highest value coin UNDER actual _highestValueCoin
        
        IF _highestValueCoin == NULL:
            NO SOLUTION POSSIBLE
            BREAK

        CONTINUE
    
    DECREMENT number of coins of _highestValueCoin
    UPDATE _total
       
    IF _total < change:
        INCREMENT number of coins of _highestValueCoin
        UPDATE _total
        _highestValueCoin = highest value coin UNDER actual _highestValueCoin
        IF _highestValueCoin == NULL:
            NO SOLUTION POSSIBLE
            BREAK
```
    
# Program

Allow to test the algorithm

Takes multiple arguments:

- customerInput: Float
- list of coins

Example :
args = 2.8f 2.0f 0 1.0f 2 0.5f 4 0.10f 8

customerInput = 2.8f
coins = [<2.0, 0>, <1.0, 2>, <0.5, 4>, <0.1, 8>]


# Example : how to run in cmd line (having jdk 13)

mvn clean package

mvn exec:java -Dexec.mainClass="com.brthao.qiminfo.Program" -Dexec.args="2.8f 2.0f 0 1.0f 2 0.5f 4 0.10f 8"   


