package com.brthao.qiminfo;

import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Bryan
 */
public class VendingMachineTest {
    
    @Test
    public void testSumCoins(){
        Map coins = Map.of(2.0f, 1, 1.0f, 2, 0.5f, 2);
        
        Float sum = VendingMachine.sumCoins(coins);
        
        Assert.assertEquals(5.0f, sum, 0.0001);
    }
    
    @Test
    public void testGetNbCoins(){
        Map coins = Map.of(2.0f, 1, 1.0f, 2, 0.5f, 2);
        
        int nbCoins = VendingMachine.getNbCoins(coins);
        
        Assert.assertEquals(5, nbCoins);
    }
    
    
    @Test
    public void testReturnChangeMaxCoins(){
        Map coins = Map.of(2.0f, 0, 1.0f, 2, 0.5f, 4, 0.10f, 8);
        VendingMachine vm = new VendingMachine(coins);
        
        Map<Float, Integer> finalChangeCoins = vm.returnChangeMaxCoins(2.8f);
        Float sumChange = VendingMachine.sumCoins(finalChangeCoins);
        int nbCoinsChange = VendingMachine.getNbCoins(finalChangeCoins);
        
        Assert.assertEquals(2.0f, sumChange, 0.0001);
        Assert.assertEquals(8, nbCoinsChange);
    }
    
}
