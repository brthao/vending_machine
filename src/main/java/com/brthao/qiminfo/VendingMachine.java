package com.brthao.qiminfo;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Bryan
 */
public class VendingMachine {

    /**
     * The coins in the vending machine are represented by a Map.
     * <type of coin, number of instance>
     *
     * example : [<2.0f,2>, <1.0f, 4>, <0.50f, 11>]
     */
    private Map<Float, Integer> coins;

    /*
        Rule defined for the problem : all the products have the same price.
     */
    public static final Float ALL_PRODUCTS_PRICE = 0.80f;
    
    /**
     *  Allow us to compare Float in the precision we need
     */
    public static final double THRESHOLD = .001;

    public VendingMachine(Map<Float, Integer> coins) {
        this.coins = coins;
    }

    public Map<Float, Integer> getCoins() {
        return coins;
    }

    public void setCoins(Map<Float, Integer> coins) {
        this.coins = coins;
    }

    /**
     * Calculate the sum of all the coins for the given input
     *
     * @param coins map containing coins of different values with their number
     * of instance
     * @return Float representing the sum of all the coins in the given input
     */
    public static Float sumCoins(Map<Float, Integer> coins) {
        Float sum = 0.0f;
        return coins.entrySet().stream()
                .map((entry) -> entry.getKey() * entry.getValue())
                .reduce(sum, (accumulator, _item) -> accumulator + _item);
    }

    /**
     * Calculate the number of coins in the given input
     *
     * @param coins map containing coins of different values with their number
     * of instance
     * @return int representing the number of coins in the given input
     */
    public static int getNbCoins(Map<Float, Integer> coins) {
        int nbCoins = 0;
        return coins.entrySet().stream()
                .map((entry) -> entry.getValue())
                .reduce(nbCoins, (accumulator, _item) -> accumulator += _item);
    }

    /**
     * Return the change given the input, using the most coins possible.
     *
     * @param customerInput
     * @return the coins for the change
     */
    public Map<Float, Integer> returnChangeMaxCoins(Float customerInput) {

        TreeMap<Float, Integer> sortedMap = new TreeMap(this.coins);
        Float sumCoins = VendingMachine.sumCoins(sortedMap);
        Float change = customerInput - VendingMachine.ALL_PRODUCTS_PRICE;

        /*
            Not handling when customer input is not enough to buy a product
         */
        if (Float.compare(change, 0.0f) < 0) {
            throw new IllegalArgumentException();
        }

        Float actualCoinType = sortedMap.lastKey();
        Integer actualNbCoins = sortedMap.get(actualCoinType);

        /*
            While sumCoins != change
            Need to do it this way because we computed sumCoins
        */
        while (Math.abs(sumCoins - change) > VendingMachine.THRESHOLD) {
            
            if(actualNbCoins == 0){
                /*
                    Already in the lowest value coin
                */
                if(Float.compare(actualCoinType, sortedMap.firstKey()) == 0){
                    System.out.println("NO SOLUTION POSSIBLE");
                    return Map.of();
                }
                
                actualCoinType = sortedMap.lowerKey(actualCoinType);
                actualNbCoins = sortedMap.get(actualCoinType);
                continue;
            }
            
            sortedMap.put(actualCoinType, sortedMap.get(actualCoinType) - 1);
            actualNbCoins = sortedMap.get(actualCoinType);
            sumCoins -= actualCoinType;
            
            if(Float.compare(sumCoins, change) < 0){
                
                sortedMap.put(actualCoinType, sortedMap.get(actualCoinType) + 1);
                sumCoins += actualCoinType;
                /*
                    Already in the lowest value coin
                */
                if(Float.compare(actualCoinType, sortedMap.firstKey()) == 0){
                    System.out.println("NO SOLUTION POSSIBLE");
                    return Map.of();
                }
                
                actualCoinType = sortedMap.lowerKey(actualCoinType);
                actualNbCoins = sortedMap.get(actualCoinType);
            }
            
        }

        return sortedMap;
    }

}
