/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.brthao.qiminfo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Bryan
 */
public class Program {
    
    public static void main(String[] args){
        Map<Float, Integer> m;
        Float customerInput;
//        if(args.length % 2 == 0) throw new IllegalArgumentException();
        if(args.length < 3){
            m = Map.of(2.0f, 0, 1.0f, 2, 0.5f, 4, 0.10f, 8);
            customerInput = 2.8f;
        }
        else{
            customerInput = Float.valueOf(args[0]);
            m = new HashMap<>();
            for(int i=1; i<args.length-1; i+=2){
                m.put(Float.valueOf(args[i]), Integer.valueOf(args[i+1]));
            }

        }
        
        System.out.println("Product's price = " + VendingMachine.ALL_PRODUCTS_PRICE);
        System.out.println("Customer input : " + customerInput);
        System.out.println("Change to return = " + (customerInput-VendingMachine.ALL_PRODUCTS_PRICE));
        System.out.println("");
        
        VendingMachine vm = new VendingMachine(m);
        System.out.println("Start vending machine : ");
        vm.getCoins().entrySet().forEach((entry) -> System.out.println(entry.getKey() + " => " + entry.getValue()));

        Map<Float, Integer> change = vm.returnChangeMaxCoins(customerInput);

        
        System.out.println("");
        System.out.println("Change : ");
        change.entrySet().forEach((entry) -> System.out.println(entry.getKey() + " => " + entry.getValue()));
        
        System.out.println("Coins returned = " + VendingMachine.getNbCoins(change));
//        Arrays.stream(args).forEach(System.out::println);
    }
}
